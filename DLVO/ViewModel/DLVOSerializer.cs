﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace DLVO
{
    /// <summary>
    /// Class takes a DlvoModel type model and turns it into a string of XML, or vise versa
    /// </summary>
    public class DlvoSerializer
    {
        private XmlSerializer xmlserializer = new XmlSerializer(typeof(DlvoModel));

        public string Serialize(DlvoModel model)
        {
            using (var sww = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(sww))
                {
                    xmlserializer.Serialize(writer, model);
                    return sww.ToString();
                }
            }
        }

        public DlvoModel Deserialize(FileStream rawStream)
        {
            var model = new DlvoModel();
            StreamReader SR = new StreamReader(rawStream);
            using (TextReader TextReader = new StringReader(SR.ReadToEnd()))
            {
                model = (DlvoModel)xmlserializer.Deserialize(TextReader);
            }
            return model;
        }
    }
}
