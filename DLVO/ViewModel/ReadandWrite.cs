﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;


namespace DLVO
{
    public class ReadandWrite
    {
        public static DlvoModel ReadDlvoModelXmlFile(string filePath)
        {
            DlvoSerializer deserializer = new DlvoSerializer();

            try
            {
                FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate);
                return deserializer.Deserialize(fs);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public static void WriteDlvoModelXmlFile(DlvoModel model, string fileName)
        {
            DlvoSerializer serializer = new DlvoSerializer();
            string modelXml = serializer.Serialize(model);
            XmlDocument xdoc = new XmlDocument();
            xdoc.LoadXml(modelXml);
            xdoc.Save(fileName);
        }
    }
}
