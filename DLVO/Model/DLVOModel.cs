﻿using System.Collections.Generic;

namespace DLVO
{
    /// <summary>
    /// DLVOModel should have no connection with the ViewModel. In this class, ViewModel should never be mentioned
    /// This is only a database with properties
    /// General properties should be clusterd according to general specifics
    /// While formula specific properties should be clusterd according to formula
    /// This class is serialized so every class or property name changed influences the abilty to open an XML file
    /// </summary>
    public class DlvoModel
    {
        #region General Properties
        // Plotter
        public double HMax { get; set; }
        public int Resolution { get; set; }

        // Function used
        public bool VanderWaals { get; set; }
        public bool DoubleLayer { get; set; }
        public bool BornRepulsion { get; set; }
        public bool RepulsiveHydration { get; set; }
        public bool DLVOFunction { get; set; }
        public bool ExtDLVOFunction { get; set; }
        public bool Energy { get; set; }

        //Particle parameters
        public double ParticleRadius { get; set; }
        public double SurfacePotentialParticle { get; set; }
        public double ParticleForceConstant { get; set; } //C 1
        public double ParticleDecayLength { get; set; } // Lambda 1
        #endregion

        #region Formula Specific Properties
        //Van der waals
        public double Hamaker { get; set; }

        //double layer
        public double ElementaryChargeConstant { get; set; }
        public double InverseDebyeLength { get; set; }
        public double DielectricConstantFluid { get; set; }
        public double Z { get; set; }
        public double SurfacePotentialWall { get; set; }
        public double Temperature { get; set; }
        public double IonicStrength { get; set; }

        //Born repulsion
        public double BornCollisionRange { get; set; }

        //Repulsive hydration
        public double WallForceConstant { get; set; } //c 2
        public double WallDecayLength { get; set; } //Lambda 2
        #endregion

        #region Constants //typical values, hardcoded
        public double AvogadroConstant { get; set; } = 6.02214086E23;
        public double BoltzmannConstant { get; set; } = 1.38064852E-23;
        public double Lambda { get; set; } = 1E-7; 
        public double Vacuumpermittivity { get; set; } = 8.854187817E-12;
        #endregion

        #region multisettings
        //public bool MultipleGraphs { get; set; }
        //public List<VariableParameterModel> VariableParameters { get; set; }

        #endregion
    }
    //public class VariableParameterModel
    //{
    //    public string PropertyName { get; set; }
    //    public double Value { get; set; }
    //    public VariableParameterModel(string propertyName, double value)
    //    {
    //        PropertyName = propertyName;
    //        Value = value;
    //    }
    //    public VariableParameterModel() { }
    //}
}
