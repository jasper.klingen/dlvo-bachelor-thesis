﻿using DLVO.ViewModel;
using System;
using System.Windows.Input;

namespace DLVO.Command
{
    internal class ClearPlotCommand : ICommand
    {
        private DlvoViewModel dLVOViewModel;

        public ClearPlotCommand(DlvoViewModel DLVOViewModel)
        {
            dLVOViewModel = DLVOViewModel;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            dLVOViewModel.ClearPlot();
        }
    }
}
