﻿using System;
using System.Windows.Input;

namespace DLVO.ViewModel
{
    internal class OpenCommand : ICommand
    {
        private DlvoViewModel dLVOViewModel;

        public OpenCommand(DlvoViewModel DLVOViewModel)
        {
            dLVOViewModel = DLVOViewModel;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            dLVOViewModel.Open();
        }
    }
}