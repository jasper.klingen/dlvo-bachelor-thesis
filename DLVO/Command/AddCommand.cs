﻿using System;
using System.Windows.Input;

namespace DLVO.ViewModel
{
    internal class AddCommand : ICommand
    {
        private DlvoViewModel dlvoViewModel;

        public AddCommand(DlvoViewModel dlvoViewModel)
        {
            this.dlvoViewModel = dlvoViewModel;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            //dlvoViewModel.AddToParameterList();
        }
    }
}