﻿using DLVO.ViewModel;
using System;
using System.Windows.Input;

namespace DLVO.Command
{
    internal class PlotCommand : ICommand
    {
        private DlvoViewModel dLVOViewModel;

        public PlotCommand(DlvoViewModel dLVOViewModel)
        {
            this.dLVOViewModel = dLVOViewModel;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            dLVOViewModel.Plot();
        }
    }
}