﻿using System;
using System.Windows.Input;

namespace DLVO.ViewModel
{
    internal class SaveCommand : ICommand
    {
        private DlvoViewModel dLVOViewModel;

        public SaveCommand(DlvoViewModel dLVOViewModel)
        {
            this.dLVOViewModel = dLVOViewModel;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            dLVOViewModel.Save();
        }
    }
}